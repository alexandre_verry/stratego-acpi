package type;

public enum TypePiece {
    //Piece ( attackPoint, numberPerPlayer )
    MARSHAL(10,1),
    GENERAL(9,1),
    COLONEL(8,2),
    MAJOR(7,3),
    CAPTAIN(6,4),
    LIEUTENANT(5,4),
    SERGEANT(4,4),
    SAPPER(3,5),
    SCOUT(2,8),
    SPY(1,1),
    BOMB(0,6),
    FLAG(0,1);

    private int attackPoints;
    private int numberPerPlayer;

    /**
     * constructor of typePiece
     * @param ap
     * @param numberPerPlayer
     */
    TypePiece(int ap, int numberPerPlayer)
    {
        this.attackPoints=ap;
        this.numberPerPlayer = numberPerPlayer;
    }

    /**
     *
     * @return attack point of the piece
     */
    public int getAttackPoints(){
        return this.attackPoints;
    }

    /**
     *
     * @return number of piece allow for each player
     */
    public int getNumberPerPlayer(){
        return this.numberPerPlayer;
    }
}
