package view;

import bin.Stratego;
import controller.MoveController;
import model.Case;
import type.TypeCase;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: elylebesson
 * Date: 03/12/13
 * Time: 10:53
 * To change this template use File | Settings | File Templates.
 */

public class CaseView extends JPanel{
    Logger logger = Logger.getLogger(Stratego.class.getName());
    private boolean selected;
    private int m_x;
    private int m_y;
    /**
     * CaseView Contrustor
     * @param c Case witch point on (model) Case
     */
    public CaseView(final Case c) {
        this.m_x = c.getCoordonate().getX();
        this.m_y = c.getCoordonate().getY();
    }

    /**
     *
     * @return
     */
    public int getM_x() {
        return m_x;
    }

    /**
     *
     * @param m_x
     */
    public void setM_x(int m_x) {
        this.m_x = m_x;
    }

    /**
     *
     * @return
     */
    public int getM_y() {
        return m_y;
    }

    /**
     *
     * @param m_y
     */
    public void setM_y(int m_y) {
        this.m_y = m_y;
    }

    /**
     *
     * @return
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     *
     * @param selected
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
