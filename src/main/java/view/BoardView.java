package view;

import controller.MoveController;
import model.Board;
import model.Case;
import service.TranslateService;
import type.TypeCase;
import type.TypePiece;
import type.TypePlayer;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: elylebesson
 * Date: 03/12/13
 * Time: 10:35
 * To change this template use File | Settings | File Templates.
 */
public class BoardView extends JPanel {

    //Logger logger = Logger.getLogger(Stratego.class.getName());
    private PieceView pieceSelected;
    private CaseView[][] cases = new CaseView[10][10];
    private CaseView selectedCase = null;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private ArrayList<PieceView> listPiecesView = new ArrayList<PieceView>();

    private Border blackLine = BorderFactory.createLineBorder(Color.black,1);

    /**
     * Init windows cases
<<<<<<< HEAD
     * @param matriceCases all of board's cases
     * @param players
     */
    public BoardView(Case[][] matriceCases, final List<Player> players){

        for ( Player player : players) {
=======
     * @param board
     */
    public BoardView(final Board board){
        final Case[][] matriceCases = board.getMatrixOfCase();
>>>>>>> bf0852f18a60a29d6c9f3f7e561f31b8663a1b79

            for(int i = 0; i<matriceCases.length; i++){
                for(int j=0; j<matriceCases[i].length; j++) {

<<<<<<< HEAD
                    CaseView m_case = new CaseView(matriceCases[i][j]);
                    m_case.setBorder(blackLine);
                    if ( m_case.isSelected() == true ){
                        selectedCase = m_case;
                    }
=======
                CaseView m_case = new CaseView(matriceCases[i][j]);
                m_case.setBorder(blackLine);

                PieceView m_piece = null;
                m_case.setBorder(blackLine);
>>>>>>> bf0852f18a60a29d6c9f3f7e561f31b8663a1b79

                    if(matriceCases[i][j].getTypeCase().equals(TypeCase.DISABLED)){
                        m_case.setBackground(Color.black);
                    }
                    m_case.addMouseListener(new MouseAdapter() {
                        public void mousePressed(MouseEvent e) {
                            JPanel source = (JPanel) e.getSource();

<<<<<<< HEAD
                            for(PieceView pv : listPiecesView) {
                                if(pv.isSelected()){
                                    pv.getParent().remove(pv);
                                    source.add(pv);
                                    pv.setBorder(blackLine);
                                    pv.setSelected(false);
                                    repaint();
                                }
                            }
                        }
                    });
                    cases[i][j] = new CaseView(matriceCases[i][j]) ;
=======
                if(matriceCases[i][j].getPiece()!=null){
                    m_piece = new PieceView(matriceCases[i][j].getPiece());
                    m_case.add(m_piece);
>>>>>>> bf0852f18a60a29d6c9f3f7e561f31b8663a1b79
                }
                this.add(m_case);
                /////////
                //  Listener
                /////////
                if(board.getMatrixOfCase()[m_case.getM_x()][m_case.getM_y()].getTypeCase() != TypeCase.DISABLED) {
                    m_case.addMouseListener(new MouseAdapter() {
                        public void mousePressed(MouseEvent e) {
                            displayBoardUpdated(board);
                            CaseView endCase = (CaseView) e.getSource();
                            CaseView beginCase = null;

<<<<<<< HEAD

            }
            for(int i = 0; i<cases.length; i++){
                for(int j=0; j<cases[i].length; j++) {
                    this.add(cases[i][j]);
                }
            }

            for(int i=0; i<player.getListPiece().size(); i++){
                listPiecesView.add(new PieceView(player.getListPiece().get(i)));
            }
            if ( player.getTypePlayer().equals(TypePlayer.BLUE)){
                for ( Piece piece : player.getListPiece()){
                    for(int i = 0; i<matriceCases.length - player.getListPiece().size(); i++){
                        for(int j=0; j<matriceCases[i].length - player.getListPiece().size(); j++) {
                            cases[i][j].add(new PieceView(piece));
=======


                            int beginX = 0;
                            int beginY = 0;

                            int endX =  endCase.getM_x();
                            int endY =  endCase.getM_y();

                            for(int i=0; i<10; i++)
                                for(int j=0; j<10; j++){
                                    if(cases[i][j].isSelected()){
                                        beginX = cases[i][j].getM_x();
                                        beginY = cases[i][j].getM_y();
                                        cases[i][j].setSelected(false);
                                    }
                                }

                            MoveController mc = new MoveController();
                            mc.doDisplacement(matriceCases[beginX][beginY], matriceCases[endX][endY], board);

                            displayBoardUpdated(board);
                            JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(endCase);
                            revalidate();
                            repaint();
>>>>>>> bf0852f18a60a29d6c9f3f7e561f31b8663a1b79
                        }

                    });
                }
            }
        }
    }

    /**
     *
     * @param board
     */
    public void displayBoardUpdated(Board board){
        boolean flagBlue = false;
        boolean flagRed = false;
        TypePlayer winner = null;

        Component[] compoCaseV = this.getComponents();
        for(int i=0; i<compoCaseV.length; i++){
            CaseView caseV = (CaseView)compoCaseV[i];
            caseV.removeAll();
            this.cases[caseV.getM_x()][caseV.getM_y()] = caseV;
        }

        for(int i=0; i<10; i++){
            for(int j=0; j<10; j++){
                if(board.getMatrixOfCase()[i][j].getPiece() != null){
                    cases[i][j].add(new PieceView(board.getMatrixOfCase()[i][j].getPiece()));
                    PieceView p = (PieceView)cases[i][j].getComponent(0);
                    if(board.getMatrixOfCase()[i][j].getPiece().getTypePiece()== TypePiece.FLAG){
                        if(board.getMatrixOfCase()[i][j].getPiece().getPlayer()==TypePlayer.BLUE)
                            flagBlue = true;
                        else
                            flagRed = true;
                    }
<<<<<<< HEAD
                }
            } /*else {
//                for ( Piece piece : player.getListPiece()){
//                    for ( int l = cases.length ; l > 0 ; l--){
//                        cases[l].add(new PieceView(piece));
//                    }
//                }
            } */
=======

                }

            }
        }
        if(flagBlue && !flagRed){
            winner = TypePlayer.BLUE;
        }
        else if(!flagBlue &&  flagRed){
            winner = TypePlayer.RED;
        }
        if(winner != null){
            removeAll();
            JPanel panelInfo = new JPanel();
            JLabel lblInfo = new JLabel("Le joueur " + TranslateService.translate(winner.name()) + " a gagné !");
            this.add(lblInfo);
            //frame.add("South", panelInfo);
            revalidate();
            repaint();
        }
>>>>>>> bf0852f18a60a29d6c9f3f7e561f31b8663a1b79

        }
    }
}
