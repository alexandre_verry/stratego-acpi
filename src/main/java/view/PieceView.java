package view;

import model.Piece;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;

/**
 * Created with IntelliJ IDEA.
 * User: elylebesson
 * Date: 03/12/13
 * Time: 16:52
 * To change this template use File | Settings | File Templates.
 */
public class PieceView extends JPanel {
    private boolean selected;

    private JLabel lblName;
    private JLabel lblRank;

    private Border blackLine = BorderFactory.createLineBorder(Color.BLACK,1);
    private Border greenLine = BorderFactory.createLineBorder(Color.GREEN,1);
    private Font fontName = new Font("Arial",Font.BOLD,10);
    private Font fontRank = new Font("Arial",Font.BOLD,24);

    /**
     * PieceView Constructor
     * @param piece
     */
    public PieceView(Piece piece){

        this.setLayout(new BorderLayout());

        lblName = new JLabel(piece.getTypePiece().name());
        lblRank = new JLabel(Integer.toString(piece.getTypePiece().getAttackPoints()), JLabel.CENTER);
        lblName.setForeground(Color.WHITE);
        lblRank.setForeground(Color.WHITE);
        lblName.setFont(fontName);
        lblRank.setFont(fontRank);

        this.setBorder(blackLine);

        if(piece.getPlayer().name()=="BLUE"){
            this.setBackground(Color.BLUE);
        }
        else {
            this.setBackground(Color.RED);
        }

        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                PieceView source = (PieceView) e.getSource();
                CaseView c = (CaseView)source.getParent();
                if (selected){
                    source.setBorder(blackLine);
                    selected = false;
                    c.setSelected(false);

                }
                else {
                    source.setBorder(greenLine);
                    selected = true;
                    c.setSelected(true);
                }
            }
        });

        this.setPreferredSize(new Dimension(50,50));
        this.add("North", lblName);

        if(piece.getTypePiece().name()!="BOMB" && piece.getTypePiece().name()!="FLAG"){
            this.add("Center", lblRank);
        }
    }

    /**
     * Getter of Selected boolean
     * @return boolean selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Setter of Selected boolean
     * @param selected
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
