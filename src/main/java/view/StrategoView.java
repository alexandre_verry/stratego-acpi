package view;

import bin.Stratego;
import controller.MoveController;
import model.Board;
import model.Case;
import model.Piece;
import model.Player;
import type.TypePiece;
import type.TypePlayer;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StrategoView extends JFrame {
    private Case[][] m_matriceCase;
    private Board m_board;
    private List<Player> players;

    Logger logger = Logger.getLogger(Stratego.class.getName());

    private Border blackLine = BorderFactory.createLineBorder(Color.black,1);

    /**
     *
     * @param board
     * @param players
     */
    public StrategoView(Board board, List<Player> players) {
        super();
        this.m_board = board;
        this.m_matriceCase = board.getMatrixOfCase();
        this.players = players;
        configFrame();
        panelManager(players);
        this.setVisible(true);
        while (m_board.getWinner() == null){

        }
        System.out.print("lrdohkdxflighdf");
        JPanel panelInfo = new JPanel();
        JLabel lblInfo = new JLabel("Un joueur a gagné !");
        panelInfo.add(lblInfo);
        this.add("South", panelInfo);
        revalidate();
        repaint();
        dispose();
    }

    private Player victorious() {
        boolean hasFlag = false;
        int looser = -1;
        for ( int i = 0 ; i < players.size() ; i++){
            for ( Piece piece : players.get(i).getListPiece()){
                if ( piece.getTypePiece() == TypePiece.FLAG){
                    hasFlag = true;
                }
            }
            if ( hasFlag == false ){
                looser = i;
            }
        }
        if  ( looser == 1 ){
            return players.get(0);
        } else if ( looser == 0 ){
            return players.get(1);
        }

        return null;
    }


    /**
     *  Create windows
     */
    private void configFrame(){
        setTitle("Stratego");
        setSize(1024, 768);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void repaint() {
        super.repaint();

    }

    private void panelManager(List<Player> players){
        this.setLayout(new BorderLayout());

        ///////////
        //   Init Game
        //////////

        ArrayList<Piece> listPieceBlue = (ArrayList<Piece>) players.get(0).getListPiece();
        ArrayList<Piece> listPieceRed = (ArrayList<Piece>) players.get(1).getListPiece();

        ///////////
        //   Grid Panel
        //////////

        BoardView p_board = new BoardView(m_board);
        p_board.setLayout(new GridLayout(10,10));

        JPanel p_menu = new JPanel();
        JPanel p_menuBlue = new JPanel();
        p_menuBlue.setLayout(new GridLayout(5,4));
        JPanel p_menuRed = new JPanel();
        p_menuRed.setLayout(new GridLayout(5,4));

        p_menu.setLayout(new BorderLayout());
        p_menu.setPreferredSize(new Dimension(300,500));
        p_menu.setBorder(blackLine);

        for(int i=0; i < listPieceBlue.size(); i++){
            p_menuBlue.add(new PieceView(players.get(0).getListPiece().get(i)));
        }
        for(int i=0; i < listPieceRed.size(); i++){
            p_menuRed.add(new PieceView(players.get(1).getListPiece().get(i)));
        }

        p_menu.add("North",p_menuBlue);
        p_menu.add("South",p_menuRed);

        this.add("Center",p_board);
        this.add("East",p_menu);
    }
}
