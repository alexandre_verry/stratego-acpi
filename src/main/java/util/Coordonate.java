package util;

/**
 * Created with IntelliJ IDEA.
 * User: elylebesson
 * Date: 03/12/13
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */
public class Coordonate {
    private int x;
    private int y;

    /**
     * Return x coordonate
     * @return x coordonate
     */
    public Coordonate(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    /**
     * Set new x coordonate
     * @param x coordonate to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Return y coordonate
     * @return y coordonate
     */
    public int getY() {
        return y;
    }

    /**
     * Set new y coordonate
     * @param y coordonate to set
     */
    public void setY(int y) {
        this.y = y;
    }



}
