package bin;

import model.Board;
import model.Case;
import model.Piece;
import model.Player;
import type.TypePiece;
import type.TypePlayer;
import view.StrategoView;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Stratego {

    static Logger logger = Logger.getLogger(Stratego.class.getName());

    /**
     * Entry point
     * @param args
     */
    public static void main(String args[]) {

        logger.log(Level.INFO, "Start stratego");

        Stratego stratego = new Stratego();
        List<Player> players = stratego.initPlayers();
//        logger.log(Level.INFO, String.valueOf(players.size()));
        stratego.initListPiece(players);

        Board board = new Board();

//        Case[][] matriceCase = board.getMatrixOfCase();

        int ind = 0;
        for(int x = 0; x<4; x++){
            for(int y = 0; y<board.getMatrixOfCase()[x].length; y++) {
                board.insertPiece(x,y,players.get(0).getListPiece().get(ind));
                ind++;
            }
        }

        ind =  0;
        for(int x = 6; x<10; x++){
            for(int y = 0; y<board.getMatrixOfCase()[x].length; y++) {
                board.insertPiece(x,y,players.get(1).getListPiece().get(ind));
                ind++;
            }
        }

        StrategoView strategoView = new StrategoView(board, players);
    }

    public List<Player> initPlayers(){
        ArrayList<Player> list = new ArrayList<Player>();
        list.add(new Player(TypePlayer.BLUE));
        list.add(new Player(TypePlayer.RED));
        return list;
    }

    /**
     * Init a list of piece for player
     * @param listPlayer contains players
     */
    public void initListPiece(List<Player> listPlayer){
        for (Player player : listPlayer) {
            List<Piece> listPiece = player.getListPiece();
            for (int i = 0; i < TypePiece.CAPTAIN.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.CAPTAIN);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.MARSHAL.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.MARSHAL);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.GENERAL.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.GENERAL);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.COLONEL.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.COLONEL);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.MAJOR.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.MAJOR);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.LIEUTENANT.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.LIEUTENANT);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.SERGEANT.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.SERGEANT);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.SAPPER.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.SAPPER);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.SCOUT.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.SCOUT);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.SPY.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.SPY);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.BOMB.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.BOMB);
                listPiece.add(piece);
            }
            for (int i = 0; i < TypePiece.FLAG.getNumberPerPlayer(); i++) {
                Piece piece = new Piece(player.getTypePlayer(), TypePiece.FLAG);
                listPiece.add(piece);
            }
        }
    }

}
