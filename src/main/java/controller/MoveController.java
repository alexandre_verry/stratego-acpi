package controller;

import model.Board;
import model.Case;
import model.Piece;
import type.TypePiece;
import util.Coordonate;
import view.CaseView;

import javax.swing.*;
import java.awt.event.MouseAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: gbarazer
 * Date: 04/12/13
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */
public class MoveController {

    public MoveController() {

    }


    public void doDisplacement(Case begin, Case end, Board b){
        if (validMove(begin, end) && (begin.getPiece() != null && end.getPiece() == null)) {
            b.movePiece(end.getCoordonate(),begin.getPiece());
        }
        else if (validCombat(begin,end) && validMove(begin,end)){
            FightController fight = new FightController();
            Piece looser = fight.doFight(begin.getPiece(), end.getPiece());
            if(looser == begin.getPiece()){
                b.getCase(begin.getCoordonate().getX(),begin.getCoordonate().getY()).setPiece(null);
            }
            else if (looser == null) {
                    b.getCase(end.getCoordonate().getX(),end.getCoordonate().getY()).setPiece(null);
                    b.getCase(begin.getCoordonate().getX(),begin.getCoordonate().getY()).setPiece(null);
            }
            else if (looser.getTypePiece() == TypePiece.FLAG){
                    b.setWinner(looser.getPlayer());
                    b.getCase(end.getCoordonate().getX(),end.getCoordonate().getY()).setPiece(null);
                    b.movePiece(end.getCoordonate(), begin.getPiece());
            }
            else{
                    b.getCase(end.getCoordonate().getX(),end.getCoordonate().getY()).setPiece(null);
                    b.movePiece(end.getCoordonate(), begin.getPiece());
            }
        }
    }

    private boolean validCombat(Case begin, Case end) {
        if(begin.getPiece() != null && end.getPiece() != null){
            if(begin.getPiece().getPlayer() != end.getPiece().getPlayer())
                return true;
        }
        return false;
    }

    public boolean validMove(Case begin, Case end) {
        if (begin.getPiece() != null && begin.getPiece().getTypePiece() != TypePiece.FLAG && begin.getPiece().getTypePiece() != TypePiece.BOMB) {
            if(((begin.getCoordonate().getX() == end.getCoordonate().getX()+1 || begin.getCoordonate().getX() == end.getCoordonate().getX()-1)
                    && begin.getCoordonate().getY() == end.getCoordonate().getY()) ||
                    ((begin.getCoordonate().getY() == end.getCoordonate().getY()+1 || begin.getCoordonate().getY() == end.getCoordonate().getY()-1)
                            && begin.getCoordonate().getX() == end.getCoordonate().getX()) ){
                return true;
            } else if(begin.getPiece().getTypePiece()== TypePiece.SCOUT && (begin.getCoordonate().getX()==end.getCoordonate().getX() ||
                    begin.getCoordonate().getY() == end.getCoordonate().getY())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
