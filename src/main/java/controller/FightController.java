package controller;

import model.Piece;
import type.TypePiece;

import java.util.logging.Level;
import java.util.logging.Logger;

public class FightController {

    Logger logger = Logger.getLogger(FightController.class.getName());

    /**
     * Determine who win fight between two piece
     * @param attack
     * @param defence
     * @return looser Piece
     */
    public Piece doFight(Piece attack, Piece defence) {

        Piece looser;
        if(defence.getTypePiece()==TypePiece.MARSHAL && attack.getTypePiece()==TypePiece.SPY){ // if Spy vs Marshal
            looser = defence;
        }
        else if (defence.getTypePiece() != TypePiece.BOMB
                && defence.getTypePiece() != TypePiece.FLAG) {
            if (attack.getTypePiece().getAttackPoints() >
                    defence.getTypePiece().getAttackPoints()) { // If attack win
                looser = defence;
            } else if (attack.getTypePiece().getAttackPoints() <
                    defence.getTypePiece().getAttackPoints() ) { // If defense win
                looser = attack;
            } else { // If equality
                looser = null;
            }
        } else if ( defence.getTypePiece() == TypePiece.BOMB ){// If defence is a Bomb
            if (attack.getTypePiece() == TypePiece.SAPPER) { // If attack is a Sapper so attack win
                looser = defence;
            } else { // If attack is not a Sapper so defense win
                looser = attack;
            }
        } else {
            looser = defence; // flag
        }
        return looser;
    }
}
