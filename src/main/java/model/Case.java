package model;

import type.TypeCase;
import util.Coordonate;

public class Case {

    private Piece piece;
    private Coordonate coordonate;
    private TypeCase typeCase;

    /**
     * Constructor of Case
     * @param x x coordonate
     * @param y y coordonate
     */
    public Case(int x, int y) {
        this.typeCase = TypeCase.UNSELECTED;
        this.piece = null;
        this.coordonate = new Coordonate(x,y);
    }

    /**
     *
     * @return piece
     */
    public Piece getPiece() {
        return piece;
    }

    /**
     *
     * @param piece
     */
    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    /**
     *
     * @return
     */
    public TypeCase getTypeCase() {
        return typeCase;
    }


    /**
     *
     * @param typeCase
     */
    public void setTypeCase(TypeCase typeCase) {
        this.typeCase = typeCase;
    }

    /**
     *
     */
    public Coordonate getCoordonate() {
        return coordonate;

    }
}
