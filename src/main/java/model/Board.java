package model;

import type.TypeCase;
import type.TypePlayer;
import util.Coordonate;

import java.util.Arrays;

public class Board {
    private Case matrixOfCase[][];
    private TypePlayer winner = null;

    public TypePlayer getWinner() {
        return winner;
    }

    public void setWinner(TypePlayer winner) {
        this.winner = winner;
    }

    /**
     * Constructor of the board
     */
    public Board() {
        this.matrixOfCase = new Case[10][10];
        for(int i=0; i<10; i++){
            for(int j=0; j<10; j++){
                this.matrixOfCase[i][j] = new Case(i,j);
            }
        }
        // set the two area (lakes) to disable: no Piece can go there
        for(int i=0; i<2; i++){
            this.matrixOfCase[4+i][2].setTypeCase(TypeCase.DISABLED);
            this.matrixOfCase[4+i][3].setTypeCase(TypeCase.DISABLED);

            this.matrixOfCase[4+i][6].setTypeCase(TypeCase.DISABLED);
            this.matrixOfCase[4+i][7].setTypeCase(TypeCase.DISABLED);
        }
    }

    /**
     *
     * @return 2D table represent the board
     */
    public Case[][] getMatrixOfCase() {
        return matrixOfCase;
    }

    /**
     *
     * @param x
     * @param y
     * @return the Case who is contains in the board at the coordinate [x,y]
     */
    public Case getCase(int x, int y){
        return this.matrixOfCase[x][y];
    }

    /**
     *
     * @param x
     * @param y
     * @param c
     * @return boolean: true if the Piece has been inserted, false if it's impossible
     */
    public boolean insertPiece(int x, int y, Piece c){

        if(!matrixOfCase[x][y].getTypeCase().equals(TypeCase.DISABLED) && matrixOfCase[x][y].getPiece()==null){
            this.matrixOfCase[x][y].setPiece(c);
            c.setCoordonate(new Coordonate(x,y));
            return true;
        }
        else
            return false;
    }

    /**
     *
     * @param c
     * @param p
     */
    public void movePiece(Coordonate c, Piece p){
        // old case is set to null
        this.matrixOfCase[p.getCoordonate().getX()][p.getCoordonate().getY()].setPiece(null);
        // new case is set to the Piece
        this.matrixOfCase[c.getX()][c.getY()].setPiece(p);
        // Update of the coordonate of the Piece
        p.movePiece(c);

    }
}
