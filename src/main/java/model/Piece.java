package model;

import model.Player;
import type.TypePiece;
import type.TypePlayer;
import util.Coordonate;

public class Piece {
    private TypePiece typePiece;
    private TypePlayer typeplayer;
    private Coordonate coordonate;

    /**
     *
     * @param typePlayer
     * @param typePiece
     */
    public Piece(TypePlayer typePlayer, TypePiece typePiece){
        this.typeplayer = typePlayer;
        this.typePiece = typePiece;
    }

    /**
     *
     * @return type of owner
     */
    public TypePlayer getPlayer() {
        return typeplayer;
    }

    /**
     *
     * @return type piece
     */
    public TypePiece getTypePiece() {
        return typePiece;
    }

    /**
     *
     * Return coordonate
     * @return coordonate
     */
    public Coordonate getCoordonate() {
        return coordonate;
    }

    /**
     *
     * @param coordonate
     */
    public void setCoordonate(Coordonate coordonate) {
        this.coordonate = coordonate;
    }

     /**
     * Set new coordonate
     * @param c
     */
    public void movePiece(Coordonate c) {
        this.coordonate.setX(c.getX());
        this.coordonate.setY(c.getY());
    }

}