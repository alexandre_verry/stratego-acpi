package model;

import type.TypePlayer;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private TypePlayer typePlayer;
    private List<Piece> listPiece;

    public boolean isLooser() {
        return looser;
    }

    public void setLooser(boolean victorious) {
        this.looser = victorious;
    }

    private boolean looser = false;

    /**
     * Constructor of player
     * @param nbPlayer to set
     */
    public Player(TypePlayer nbPlayer){
        this.typePlayer = nbPlayer;
        listPiece = new ArrayList<Piece>();
    }

    /**
     *
     * @return the type of the player
     */
    public TypePlayer getTypePlayer() {
        return typePlayer;
    }

    /**
     * Return the list of pieces
     * @return list of pieces
     */
    public List<Piece> getListPiece() {
        return listPiece;
    }



}
