package controller;

import model.Board;
import model.Case;
import model.Piece;
import org.junit.Test;
import type.TypePiece;
import type.TypePlayer;

public class MoveControllerTest {

    @Test
    public void testValidMove() {
        Board board = new Board();
        Piece piece1 = new Piece(TypePlayer.BLUE, TypePiece.MARSHAL);
        MoveController mc = new MoveController();
        board.insertPiece(1,1,piece1);

        //Story 1 Working move
        Case caseBegin = board.getCase(1,1);
        Case caseEnd = board.getCase(1,2);
        mc.doDisplacement(caseBegin,caseEnd,board);

        assert(caseBegin.getPiece() == null);
        assert(caseEnd.getPiece() == piece1);

        //Story 2 too far move
        Board board2 = new Board();
        Piece piece2 = new Piece(TypePlayer.RED, TypePiece.COLONEL);
        MoveController mc2 = new MoveController();
        board.insertPiece(1,1,piece2);

        Case caseBegin2 = board.getCase(1,1);
        Case caseEnd2 = board.getCase(2,2);
        mc2.doDisplacement(caseBegin2,caseEnd2,board2);
        assert(caseBegin2.getPiece() == piece2);
        assert(caseEnd2.getPiece() == null);


        Board boardStory4 = new Board();
        Piece pieceStory4 = new Piece(TypePlayer.BLUE, TypePiece.MARSHAL);
        boardStory4.insertPiece(2, 2, pieceStory4);
        MoveController mcStory4 = new MoveController();
        Case beginStory4 = boardStory4.getCase(3,3);
        Case endStory4 = boardStory4.getCase(2,2);
        mcStory4.doDisplacement(beginStory4, endStory4, boardStory4);

        assert(boardStory4.getCase(3,3).getPiece() == null);
        assert(boardStory4.getCase(2,2).getPiece() == pieceStory4);

    }

    @Test
    public void testScootMove(){
        //Story 3 scoot move
        Piece piece1 = new Piece(TypePlayer.RED,TypePiece.SCOUT);
        Board board3 = new Board();
        MoveController mc3 = new MoveController();
        board3.insertPiece(1,1,piece1);

        mc3.doDisplacement(board3.getCase(1,1),board3.getCase(1,9),board3);
        assert(board3.getCase(1,1).getPiece()==null);
        assert(board3.getCase(1,9).getPiece()==piece1);
    }

    @Test
    public void testCombatDeplacement(){

        Board board = new Board();
        Piece piece1 = new Piece(TypePlayer.BLUE, TypePiece.MARSHAL);
        Piece piece2 = new Piece(TypePlayer.RED, TypePiece.CAPTAIN);
        MoveController mc = new MoveController();
        board.insertPiece(1,1,piece1);
        board.insertPiece(1,2,piece2);

        //Story 1
        mc.doDisplacement(board.getCase(1,1),board.getCase(1,2),board);
        assert(board.getCase(1,1).getPiece()==null);
        assert(board.getCase(1,2).getPiece()==piece1);
    }

    @Test
    public void testValid(){
        Board board = new Board();
        Piece piece1 = new Piece(TypePlayer.BLUE, TypePiece.MARSHAL);
        Piece piece2 = new Piece(TypePlayer.RED, TypePiece.CAPTAIN);
        MoveController mc = new MoveController();
        board.insertPiece(2,2,piece2);
        mc.validMove(board.getCase(3,3), board.getCase(2,2));
        assert(board.getCase(3,3).getPiece() == null);
        assert(board.getCase(2,2).getPiece() == piece2);

    }

}
