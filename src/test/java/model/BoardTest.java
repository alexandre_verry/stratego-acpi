package model;

import org.junit.Test;
import type.TypeCase;
import type.TypePiece;
import type.TypePlayer;
import util.Coordonate;

public class BoardTest {

    @Test
    public void insertPieceTest(){
        Board myBoard = new Board();
        Piece p = new Piece(TypePlayer.BLUE, TypePiece.MAJOR);
        // Test insert piece in empty case
        myBoard.insertPiece(8,2,p);
        assert(myBoard.getCase(8,2).getPiece()).equals(p);
        // Test insert piece in already use case
        assert(myBoard.insertPiece(8,2,p) == false);
        // Test insert piece in disable case
        myBoard.getCase(8,3).setTypeCase(TypeCase.DISABLED);
        myBoard.insertPiece(8,3,p);
        assert(myBoard.getCase(8,3).getPiece() == null);
    }


    @Test
    public void moovePieceTest(){
        Board myBoard = new Board();
        Piece p = new Piece(TypePlayer.BLUE, TypePiece.MAJOR);

        myBoard.insertPiece(8,2,p);
        myBoard.movePiece(new Coordonate(8,3),p);
        assert(myBoard.getCase(8,3).getPiece()).equals(p);
        assert(myBoard.getCase(8,2).getPiece()==null);
    }


}
